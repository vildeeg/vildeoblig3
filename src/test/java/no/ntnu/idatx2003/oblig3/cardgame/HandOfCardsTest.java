package no.ntnu.idatx2003.oblig3.cardgame;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;

class HandOfCardsTest {

  @Test
  void checkNumberOfHeart() {
    HandOfCards hand = new HandOfCards();
    hand.getHand().add(new PlayingCard('H', 1));
    hand.getHand().add(new PlayingCard('S', 2));
    hand.getHand().add(new PlayingCard('H', 3));
    hand.getHand().add(new PlayingCard('C', 4));
    hand.getHand().add(new PlayingCard('H', 5));
    assertEquals(3, hand.checkHearts());
  }

  @Test
  void checkIfContainsQueenSpades() {
    HandOfCards hand = new HandOfCards();
    hand.getHand().add(new PlayingCard('H', 1));
    hand.getHand().add(new PlayingCard('S', 2));
    hand.getHand().add(new PlayingCard('H', 3));
    hand.getHand().add(new PlayingCard('C', 4));
    hand.getHand().add(new PlayingCard('S', 12));
    assertTrue(hand.checkQueenOfSpades());
  }

  @Test
  void checkIfNotContainsQueenSpades() {
    HandOfCards hand = new HandOfCards();
    hand.getHand().add(new PlayingCard('H', 1));
    hand.getHand().add(new PlayingCard('S', 2));
    hand.getHand().add(new PlayingCard('H', 3));
    hand.getHand().add(new PlayingCard('C', 4));
    hand.getHand().add(new PlayingCard('S', 11));
    assertFalse(hand.checkQueenOfSpades());
  }

  @Test
  void checkSum() {
    HandOfCards hand = new HandOfCards();
    hand.getHand().add(new PlayingCard('H', 1));
    hand.getHand().add(new PlayingCard('S', 2));
    hand.getHand().add(new PlayingCard('H', 3));
    hand.getHand().add(new PlayingCard('C', 4));
    hand.getHand().add(new PlayingCard('S', 5));
    assertEquals(15, hand.checkSum());
  }
}