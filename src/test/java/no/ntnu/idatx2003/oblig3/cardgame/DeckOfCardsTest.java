package no.ntnu.idatx2003.oblig3.cardgame;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DeckOfCardsTest {
  @BeforeEach
  void setUp() {

  }

  @Test
  void PositiveDealHand() {
    DeckOfCards deck = new DeckOfCards();
    HandOfCards hand = deck.dealHand(5);
    assertEquals(5, hand.getHand().size());

  }

  @Test
  void NegativeDealHand() {
    DeckOfCards deck = new DeckOfCards();
    HandOfCards hand = deck.dealHand(6);
    assertNotEquals(5, hand.getHand().size());
  }

  @Test
  void TestConstructor() {
    DeckOfCards deck = new DeckOfCards();
    assertEquals(52, deck.getDeck().size());
  }

}

