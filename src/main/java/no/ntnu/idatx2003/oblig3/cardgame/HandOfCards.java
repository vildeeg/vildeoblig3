package no.ntnu.idatx2003.oblig3.cardgame;

import java.util.ArrayList;
import java.util.List;

public class HandOfCards {

  private final List<PlayingCard> hand;

  public HandOfCards(){
    hand = new ArrayList<>();
  }

  public List<PlayingCard> getHand(){
    return hand;
  }

  public int checkSum(){
    return hand.stream()
        .mapToInt(PlayingCard::getFace)
        .sum();
  }




  public boolean checkQueenOfSpades(){
    return hand.stream()
        .map(PlayingCard::getAsString)
        .anyMatch(s -> s.equals("S12"));
  }

  public int checkHearts(){
    return (int) hand.stream()
        .filter(card -> card.getSuit() == 'H')
        .count();
  }

  public String toString(){
    StringBuilder string = new StringBuilder();
    for (PlayingCard card : hand) {
      string.append(card.getAsString()).append("                     ");
    }
    return string.toString();
  }
}
