package no.ntnu.idatx2003.oblig3.cardgame;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DeckOfCards {
  private final char[] suits = {'S', 'H', 'D', 'C'};
  private final List<PlayingCard> deck;

  Random random = new Random();

  public DeckOfCards(){
    deck = new ArrayList<>();
    for (char suit : suits) {
      for (int face = 1; face <= 13; face++) {
        deck.add(new PlayingCard(suit, face));
      }
    }
  }

  public List<PlayingCard> getDeck() {
    return deck;
  }

  public HandOfCards dealHand(int numberOfCards) {
    HandOfCards hand = new HandOfCards();
    for (int i = 0; i < numberOfCards; i++) {
      int cardIndex = random.nextInt(deck.size());
      hand.getHand().add(deck.get(cardIndex));
    }
    return hand;
  }

  public String toString() {
    StringBuilder string = new StringBuilder();
    for (PlayingCard card : deck) {
      string.append(card.getAsString()).append(" ");
    }
    return string.toString();
  }
}
