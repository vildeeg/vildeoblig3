package no.ntnu.idatx2003.oblig3.cardgame;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Main extends Application {

  DeckOfCards deck = new DeckOfCards();
  HandOfCards hand = new HandOfCards();
  Text sumResult = new Text();
  Text cardText = new Text();
  Text heartsText = new Text();
  Text queenSpadesText = new Text();
  Text fiveFlushText = new Text();
  StackPane root;


  public static void main(String[] args) {
    launch(args);
  }


  @Override
  public void start(Stage window) throws Exception {
    root = new StackPane();
    Scene scene = new Scene(root, 900, 600);
    window.setScene(scene);
    window.setTitle("Card Game");
    window.show();

    Rectangle board = new Rectangle(400, 300);
    board.setFill(Color.GREEN);
    board.setStroke(Color.SADDLEBROWN);
    board.setStrokeWidth(5);
    root.getChildren().add(board);

    Button dealButton = new Button("Deal");
    dealButton.setOnAction(e -> dealEvent());
    Button checkButton = new Button("Check");
    checkButton.setOnAction(e -> checkEvent());
    Button cleanButton = new Button("Clean");
    cleanButton.setOnAction(e -> cleanCards());

    root.getChildren().add(dealButton);
    root.getChildren().add(checkButton);
    root.getChildren().add(cleanButton);

    dealButton.setTranslateY(-130);
    dealButton.setTranslateX(240);
    checkButton.setTranslateY(-100);
    checkButton.setTranslateX(240);
    cleanButton.setTranslateY(-70);
    cleanButton.setTranslateX(240);

    TilePane bottomHalf = new TilePane();

    bottomHalf.getChildren().add(sumResult);
    bottomHalf.getChildren().add(heartsText);
    bottomHalf.getChildren().add(queenSpadesText);
    bottomHalf.getChildren().add(fiveFlushText);
    HBox container = new HBox();
    container.getChildren().add(bottomHalf);
    container.setAlignment(Pos.CENTER);

    root.setAlignment(Pos.CENTER);
    window.show();

  }

  private void addTextCards(){
    cardText.setText(hand.toString());
    root.getChildren().add(cardText);
    cardText.setTranslateY(0);
    cardText.setTranslateX(30);
  }

  private void dealEvent() {
    hand = deck.dealHand(5);

    for (int i = 0; i < hand.getHand().size(); i++) {
      Rectangle card = new Rectangle(30, 45);
      card.setFill(Color.WHITE);
      card.setStrokeWidth(1);
      root.getChildren().add(card);
      card.setTranslateX(-138 + 73 * i);
    }
    addTextCards();
  }

  public void checkEvent() {
    sumResult.setText("Sum:  " + hand.checkSum());
    heartsText.setText("Hearts: " + hand.checkHearts());
    queenSpadesText.setText("Queen of spades: " + hand.checkQueenOfSpades());
    root.getChildren().add(sumResult);
    sumResult.setTranslateY(180);
    sumResult.setTranslateX(-130);
    root.getChildren().add(heartsText);
    heartsText.setTranslateY(200);
    heartsText.setTranslateX(-130);
    root.getChildren().add(queenSpadesText);
    queenSpadesText.setTranslateY(220);
    queenSpadesText.setTranslateX(-130);


  }

  public void cleanCards() {
    root.getChildren().remove(4, root.getChildren().size());
  }
}
