module com.example.cardgame {
  requires javafx.controls;
  requires javafx.fxml;
  requires javafx.graphics;


  opens no.ntnu.idatx2003.oblig3.cardgame to javafx.fxml;
  exports no.ntnu.idatx2003.oblig3.cardgame;
}